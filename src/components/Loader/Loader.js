import React from 'react';
import S from './Loader.styles';

const Loader = () => <S.Loader>Loading...</S.Loader>;

export default React.memo(Loader);
