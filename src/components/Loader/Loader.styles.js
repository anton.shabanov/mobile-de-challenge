import styled from 'astroturf';

const Loader = styled('div')`
  font-weight: var(--md-font-weight-regular);
  font-size: 1.8rem;
  padding: 10px;
  text-align: center;
`;

export default {
  Loader,
};
