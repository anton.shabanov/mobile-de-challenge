import React from 'react';
import PropTypes from 'prop-types';
import S from './Page.styles';

const PageError = ({ error }) => (
  <S.PageError>
    Opps! Something wrong has happened:
    <br />({error.toString()})<br />
    Please try again later!
  </S.PageError>
);

PageError.propTypes = {
  error: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.string])
    .isRequired,
};

export default React.memo(PageError);
