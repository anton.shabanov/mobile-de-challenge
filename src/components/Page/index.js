import Page from './Page';
import PageTitle from './PageTitle';
import PageContent from './PageContent';
import PageError from './PageError';

export { Page, PageTitle, PageContent, PageError };
