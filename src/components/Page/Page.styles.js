import styled from 'astroturf';

const Page = styled('main')`
  width: 100%;
  position: relative;
`;

const PageTitle = styled('h1')`
  font-size: 2.4rem;
  font-weight: var(--md-font-weight-bold);
  color: var(--white);
  line-height: 1.1875;
  margin: 0;
  padding: var(--default-page-offset);
  background: var(--eastern-blue);

  &.center {
    text-align: center;
  }
`;

const PageContent = styled('div')`
  position: relative;
  padding: var(--default-page-offset);
`;

const PageError = styled('div')`
  font-size: 1.6rem;
  line-height: 1.5;
  font-weight: var(--md-font-weight-regular);
`;

export default {
  Page,
  PageTitle,
  PageContent,
  PageError,
};
