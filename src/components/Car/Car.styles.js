import styled from 'astroturf';
import ImageGallery from 'src/components/ImageGallery';

const Car = styled('div')`
  position: relative;
`;

const Title = styled('h1')`
  margin: 0;
  padding: 0;
  font-size: 2.6rem;
  font-weight: var(--md-font-weight-bold);
`;

const Category = styled('div')`
  margin-top: 10px;
  font-size: 1.4rem;
`;

const Price = styled('div')`
  margin-top: 10px;
  font-size: 1.8rem;
  font-weight: var(--md-font-weight-regular);
`;

const Gallery = styled(ImageGallery)`
  margin-top: 20px;
`;

export default {
  Car,
  Title,
  Category,
  Price,
  Gallery,
};
