import React from 'react';
import PropTypes from 'prop-types';
import S from './Car.styles';

const Car = ({ car }) => (
  <S.Car>
    <S.Title>{car.title}</S.Title>
    <S.Price>
      {car.price.gross} / {car.price.net}
    </S.Price>
    <S.Category>{car.category}</S.Category>
    <S.Gallery title={car.title} images={car.images} />
  </S.Car>
);

Car.propTypes = {
  car: PropTypes.shape({
    title: PropTypes.string,
    price: PropTypes.shape({
      gross: PropTypes.string,
      net: PropTypes.string,
    }),
    category: PropTypes.string,
    images: PropTypes.arrayOf(PropTypes.shape({})),
  }).isRequired,
};

export default React.memo(Car);
