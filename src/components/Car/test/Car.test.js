import React from 'react';
import { shallow } from 'enzyme';
import Car from 'src/components/Car';

describe('Components', () => {
  describe('Car', () => {
    const car = {
      title: 'Hello World',
      category: 'John, Doe',
      price: {
        gross: '100gross',
        net: '100net',
      },
      images: [{ uri: 'john-doe-img' }, { uri: 'hello-world-img' }],
    };

    it('renders car info', () => {
      const wrapper = shallow(<Car car={car} />);
      const price = [car.price.gross, car.price.net].join(' / ');

      expect(wrapper.find('Title').text()).toBe(car.title);
      expect(wrapper.find('Category').text()).toBe(car.category);
      expect(wrapper.find('Price').text()).toBe(price);
    });

    it('renders gallery with images', () => {
      const wrapper = shallow(<Car car={car} />);
      const gallery = wrapper.find('Gallery');

      expect(gallery.exists()).toBe(true);
      expect(gallery.prop('images')).toBe(car.images);
    });
  });
});
