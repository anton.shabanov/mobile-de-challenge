import React from 'react';
import { shallow } from 'enzyme';
import ImageSlider from 'src/components/ImageSlider';

describe('Components', () => {
  describe('ImageSlider', () => {
    const title = 'Hello World';
    const images = [{ uri: 'john-doe-img' }, { uri: 'hello-world-img' }];

    it('renders title if provided', () => {
      const wrapperWithoutTitle = shallow(<ImageSlider images={images} />);
      const wrapperWithTitle = shallow(
        <ImageSlider title={title} images={images} />,
      );

      expect(wrapperWithoutTitle.find('Title').exists()).toBe(false);
      expect(wrapperWithTitle.find('Title').exists()).toBe(true);
    });

    it('renders given images', () => {
      const wrapper = shallow(<ImageSlider title={title} images={images} />);

      expect(wrapper.find('Image').length).toBe(images.length);
    });

    it('renders back and next buttons', () => {
      const wrapper = shallow(<ImageSlider title={title} images={images} />);

      expect(wrapper.find('Previous').exists()).toBe(true);
      expect(wrapper.find('Next').exists()).toBe(true);
    });
  });
});
