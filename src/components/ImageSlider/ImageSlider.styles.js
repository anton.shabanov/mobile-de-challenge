import styled from 'astroturf';
import PureImage from 'src/components/Image';

const Slider = styled('div')`
  position: relative;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const Title = styled('div')`
  color: var(--white);
  background: var(--black);
  text-align: center;
  padding: 20px;
  font-size: 1.6rem;
  font-weight: var(--md-font-weight-regular);
`;

const List = styled('div')`
  flex: 1;
  display: flex;
  flex-direction: row;
  position: relative;
  transition: 0.5s;
`;

const Card = styled('div')`
  width: 100%;
  height: 100%;
  flex: 0 0 100%;
  padding: 45px;

  @media (min-width: 1024px) {
    padding: 20px;
  }
`;

const Previous = styled('a')`
  position: absolute;
  color: var(--white);
  background: var(--black);
  left: 0;
  top: 50%;
  z-index: 100;
  display: inline-block;
  padding: 10px;
  cursor: pointer;

  @media (min-width: 1024px) {
    padding: 20px;
  }
`;

const Next = styled(Previous)`
  right: 0;
  left: auto;
`;

const Image = styled(PureImage)`
  height: 100%;
  width: 100%;
`;

export default {
  Slider,
  Title,
  List,
  Card,
  Next,
  Previous,
  Image,
};
