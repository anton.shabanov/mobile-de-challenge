import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'src/components/Icon';
import { useSlider } from './hooks';
import S from './ImageSlider.styles';

const ImageSlider = ({ title, images, index, transition }) => {
  const [{ sliderStyle }, { handlePreviousClick, handleNextClick }] = useSlider(
    { images, index, transition },
  );
  return (
    <S.Slider>
      {title && <S.Title>{title}</S.Title>}
      <S.Previous onClick={handlePreviousClick}>
        <Icon name="back" />
      </S.Previous>
      <S.Next onClick={handleNextClick}>
        <Icon name="next" />
      </S.Next>
      <S.List style={sliderStyle}>
        {images.map((i, idx) => (
          <S.Card key={`${i.uri}-${idx}`}>
            <S.Image centered image={i} size="lg" />
          </S.Card>
        ))}
      </S.List>
    </S.Slider>
  );
};

ImageSlider.propTypes = {
  title: PropTypes.string,
  images: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  index: PropTypes.number,
  transition: PropTypes.number,
};

ImageSlider.defaultProps = {
  title: null,
  index: 0,
  transition: 0.4,
};

export default React.memo(ImageSlider);
