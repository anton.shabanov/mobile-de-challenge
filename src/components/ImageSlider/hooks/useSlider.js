import { useState, useCallback } from 'react';

export default ({ images, index, transition }) => {
  const [currentIndex, setCurrentIndex] = useState(index);

  const handleNextClick = useCallback(
    event => {
      const incrementedIndex = currentIndex + 1;
      const newIndex = incrementedIndex >= images.length ? 0 : incrementedIndex;

      event.preventDefault();
      setCurrentIndex(newIndex);
    },
    [currentIndex, images.length],
  );

  const handlePreviousClick = useCallback(
    event => {
      const decrementedIndex = currentIndex - 1;
      const newIndex =
        decrementedIndex < 0 ? images.length - 1 : decrementedIndex;

      event.preventDefault();
      setCurrentIndex(newIndex);
    },
    [currentIndex, images.length],
  );

  const sliderStyle = {
    transform: `translateX(${currentIndex * -100}%)`,
    transition: `${transition}s`,
  };

  return [{ sliderStyle }, { handleNextClick, handlePreviousClick }];
};
