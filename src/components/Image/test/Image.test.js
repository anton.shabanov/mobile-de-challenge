import React from 'react';
import { shallow } from 'enzyme';
import Image from 'src/components/Image/Image';

describe('Components', () => {
  describe('Image', () => {
    const image = { uri: 'john-doe-img' };
    const onClickHandler = jest.fn();

    it('renders image with correct src', () => {
      const wrapper = shallow(<Image image={image} />);

      expect(wrapper.find('Image').exists()).toBe(true);
      expect(
        wrapper
          .find('Image')
          .prop('src')
          .includes(image.uri),
      ).toBe(true);
    });

    it('should be clickable', () => {
      const wrapper = shallow(<Image image={image} onClick={onClickHandler} />);

      wrapper.simulate('click');
      expect(onClickHandler).toHaveBeenCalled();
    });
  });
});
