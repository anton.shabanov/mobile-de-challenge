import styled from 'astroturf';
import PureIcon from 'src/components/Icon';

const Link = styled('a')`
  display: block;
  position: relative;
`;

const Wrapper = styled('span')`
  display: block;
`;

const Image = styled('img')`
  max-width: 100%;
  max-height: 100%;

  &.centered {
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

const Icon = styled(PureIcon)`
  color: var(--eastern-blue);
  height: 60px;
  width: 60px;
  position: relative;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export default {
  Link,
  Image,
  Wrapper,
  Icon,
};
