import React from 'react';
import { hasObserver } from 'src/utils/browser/observer';
import Image from './Image';
import LazyImage from './LazyImage';

// eslint-disable-next-line react/display-name
export default props => {
  const Component = (hasObserver() && LazyImage) || Image;
  return <Component {...props} />;
};
