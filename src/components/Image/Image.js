import React from 'react';
import PropTypes from 'prop-types';
import { url, SIZE } from 'src/utils/image';
import S from './Image.styles';

const Image = ({ image, size, centered, ...rest }) => {
  const imageUrl = url(image, size);
  return (
    <S.Link href={imageUrl} target="_blank" rel="noopener noreferrer" {...rest}>
      <S.Image src={imageUrl} centered={centered} />
    </S.Link>
  );
};

Image.propTypes = {
  image: PropTypes.shape({}).isRequired,
  size: PropTypes.string,
  centered: PropTypes.bool,
};

Image.defaultProps = {
  size: SIZE.SMALL,
  centered: false,
};

export default React.memo(Image);
