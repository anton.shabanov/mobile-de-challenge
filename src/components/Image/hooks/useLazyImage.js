import { useEffect, useState } from 'react';
import { useIntersectionObserver } from 'src/hooks/browser';
import { preloadImage } from 'src/utils/browser/preload';
import cache from 'src/utils/browser/cache';

export default (target, imageUrl, preload = true) => {
  const [src, setSrc] = useState(null);

  const [inView] = useIntersectionObserver(target, {
    threshold: 0,
  });

  useEffect(() => {
    if (inView) {
      if (preload) {
        // let's check the cache first
        if (cache.exists(imageUrl)) {
          setSrc(imageUrl);
        } else {
          preloadImage(imageUrl).then(url => {
            cache.set(imageUrl, +new Date());
            setSrc(url);
          });
        }
      } else {
        setSrc(imageUrl);
      }
    }
  }, [inView]);

  return src;
};
