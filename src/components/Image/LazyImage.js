import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { url, SIZE } from 'src/utils/image';
import { useLazyImage } from './hooks';
import S from './Image.styles';

const LazyImage = ({ image, size, centered, ...rest }) => {
  const imageRef = useRef();
  const imageUrl = url(image, size);
  const src = useLazyImage(imageRef, imageUrl);
  return (
    <S.Link
      href={imageUrl}
      target="_blank"
      rel="noopener noreferrer"
      {...rest}
      ref={imageRef}
    >
      {!src && <S.Icon name="puff" />}
      {src && <S.Image src={src} centered={centered} />}
    </S.Link>
  );
};

LazyImage.propTypes = {
  image: PropTypes.shape({}).isRequired,
  size: PropTypes.string,
  centered: PropTypes.bool,
};

LazyImage.defaultProps = {
  size: SIZE.SMALL,
  centered: false,
};

export default React.memo(LazyImage);
