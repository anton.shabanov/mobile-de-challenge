import close from './svg/close.svg';
import back from './svg/back.svg';
import next from './svg/next.svg';
import puff from './svg/puff.svg';

// eslint-disable-next-line import/prefer-default-export
export const ICON = {
  close,
  back,
  next,
  puff,
};
