import React from 'react';
import { shallow, render } from 'enzyme';
import Icon from 'src/components/Icon';

describe('Components', () => {
  describe('Icon', () => {
    const onClickHandler = jest.fn();

    it('renders icon with svg inside', () => {
      const wrapper = render(<Icon name="back" />);
      const svg = wrapper.find('svg');

      expect(svg).not.toBe('test-file-stub');
    });

    it('should be clickable', () => {
      const wrapper = shallow(<Icon name="back" onClick={onClickHandler} />);

      wrapper.simulate('click');
      expect(onClickHandler).toHaveBeenCalled();
    });
  });
});
