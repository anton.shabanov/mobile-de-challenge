import React from 'react';
import PropTypes from 'prop-types';
import { ICON } from './constants';
import S from './Icon.styles';

const Icon = ({ name, ...rest }) => (
  <S.Icon
    id={`icon-${name}`}
    dangerouslySetInnerHTML={{ __html: ICON[name] }}
    {...rest}
  />
);

Icon.propTypes = {
  name: PropTypes.string.isRequired,
};

export default React.memo(Icon);
