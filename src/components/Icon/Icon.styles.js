import styled from 'astroturf';

const Icon = styled('div')`
  position: relative;
  display: inline-block;
  height: 22px;
  width: 22px;

  svg {
    max-width: 100%;
    max-height: 100%;
  }
`;

export default { Icon };
