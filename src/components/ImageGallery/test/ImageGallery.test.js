import React from 'react';
import { shallow } from 'enzyme';
import ImageGallery from 'src/components/ImageGallery';

describe('Components', () => {
  describe('ImageGallery', () => {
    const title = 'Hello World';
    const images = [{ uri: 'john-doe-img' }, { uri: 'hello-world-img' }];

    it('renders given images', () => {
      const wrapper = shallow(<ImageGallery title={title} images={images} />);

      expect(wrapper.find('Image').length).toBe(images.length);
    });
  });
});
