import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Modal from 'src/components/Modal';
import ImageSlider from 'src/components/ImageSlider';
import { useGallery } from './hooks';
import S from './ImageGallery.styles';

const ImageGallery = ({ title, images, ...rest }) => {
  const [
    { currentImage },
    { handleModalShow, handleModalClose },
  ] = useGallery();
  return (
    <Fragment>
      <S.Gallery {...rest}>
        {images.map((i, index) => (
          <S.Image
            key={`${i.uri}-${index}`}
            image={i}
            onClick={handleModalShow.bind(null, { image: i, index })}
          />
        ))}
      </S.Gallery>

      <Modal visible={!!currentImage} onClose={handleModalClose}>
        <ImageSlider
          title={title}
          images={images}
          index={currentImage && currentImage.index}
        />
      </Modal>
    </Fragment>
  );
};

ImageGallery.propTypes = {
  title: PropTypes.string,
  images: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

ImageGallery.defaultProps = {
  title: null,
};

export default React.memo(ImageGallery);
