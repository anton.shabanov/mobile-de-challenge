import { useState, useCallback } from 'react';

export default () => {
  const [currentImage, setCurrentImage] = useState(null);

  const handleModalShow = useCallback(({ image, index }, event) => {
    if (event) event.preventDefault();
    setCurrentImage({ image, index });
  }, []);

  const handleModalClose = useCallback(() => {
    setCurrentImage(null);
  }, []);

  return [{ currentImage }, { handleModalShow, handleModalClose }];
};
