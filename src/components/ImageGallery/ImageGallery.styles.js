import styled from 'astroturf';
import PureImage from 'src/components/Image';

const Gallery = styled('div')`
  margin: 0 -10px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const Image = styled(PureImage)`
  cursor: pointer;
  flex: 0 0 50%;
  padding: 10px;

  @media (min-width: 480px) {
    flex: 0 0 33%;
  }

  @media (min-width: 1024px) {
    flex: 0 0 220px;
  }
`;

export default {
  Gallery,
  Image,
};
