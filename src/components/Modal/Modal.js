import React from 'react';
import ReactDOM from 'react-dom';
import Icon from 'src/components/Icon';
import { useModal } from './hooks';
import S from './Modal.styles';

const modalRoot = document.getElementById('modal');

const Modal = ({ visible, onClose, children }) => {
  const el = useModal(modalRoot);

  return ReactDOM.createPortal(
    visible ? (
      <S.Modal visible={visible}>
        <S.Close onClick={onClose}>
          <Icon name="close" />
        </S.Close>
        {children}
      </S.Modal>
    ) : null,
    el.current,
  );
};

export default React.memo(Modal);
