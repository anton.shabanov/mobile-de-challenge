import { useRef, useEffect } from 'react';

export default modalRoot => {
  const el = useRef(document.createElement('div'));

  useEffect(() => {
    modalRoot.appendChild(el.current);

    return () => modalRoot.removeChild(el.current);
  }, [el]);

  return el;
};
