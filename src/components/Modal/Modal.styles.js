import styled from 'astroturf';

const Modal = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(#000, 0.9);
  display: none;

  &.visible {
    display: block;
  }
`;

const Close = styled('a')`
  font-size: 2rem;
  cursor: pointer;
  color: var(--white);
  position: absolute;
  top: 16px;
  right: 16px;
  z-index: 1;
`;

export default {
  Modal,
  Close,
};
