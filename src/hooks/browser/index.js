import useIntersectionObserver from './useIntersectionObserver';

// eslint-disable-next-line import/prefer-default-export
export { useIntersectionObserver };
