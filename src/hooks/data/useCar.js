import { useState, useEffect } from 'react';
import api from 'src/utils/api';

const fetchData = async cb => {
  try {
    const data = await api.car.show();
    cb({ error: null, data });
  } catch (e) {
    cb({ error: e, data: null });
  }
};

export default () => {
  const [data, setData] = useState({ car: null, error: null, loading: false });

  useEffect(() => {
    setData({ car: null, error: null, loading: true });

    fetchData(({ error, data: car }) => {
      setData({ car, error, loading: false });
    });
  }, []);

  return data;
};
