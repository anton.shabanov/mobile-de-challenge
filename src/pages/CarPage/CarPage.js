import React from 'react';
import { Page, PageTitle, PageContent, PageError } from 'src/components/Page';
import Loader from 'src/components/Loader';
import Car from 'src/components/Car';
import { useCar } from 'src/hooks/data';

const CarPage = () => {
  const { car, error, loading } = useCar();
  return (
    <Page>
      <PageTitle center>Car Photos</PageTitle>
      <PageContent>
        {loading && <Loader />}
        {error && <PageError error={error} />}
        {!loading && !error && car && <Car car={car} />}
      </PageContent>
    </Page>
  );
};

export default React.memo(CarPage);
