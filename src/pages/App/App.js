import React from 'react';
import CarPage from 'src/pages/CarPage';
import ErrorBoundary from 'src/components/ErrorBoundary';

const App = () => (
  <ErrorBoundary>
    <CarPage />
  </ErrorBoundary>
);

export default App;
