import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/pages/App';

// styles
import 'src/styles/app.css';

const APP_ROOT = document.getElementById('app');

ReactDOM.render(<App />, APP_ROOT);
