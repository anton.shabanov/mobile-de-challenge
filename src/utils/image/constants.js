export const PROTOCOL = 'https://';
export const SIZE = Object.freeze({
  SMALL: 'sm',
  LARGE: 'lg',
});
export const SIZE_POSTFIX = Object.freeze({
  [SIZE.SMALL]: '_2.jpg',
  [SIZE.LARGE]: '_27.jpg',
});
