import { PROTOCOL, SIZE, SIZE_POSTFIX } from 'src/utils/image/constants';
import url from 'src/utils/image/url';

describe('Utils', () => {
  describe('Image', () => {
    describe('url', () => {
      const image = {
        uri: 'dohn-doe-uri',
      };

      it('should build url with protocol', () => {
        const imageUrl = url(image);

        expect(imageUrl.startsWith(PROTOCOL)).toBeTruthy();
      });

      it('should build url with small postfix by default', () => {
        const imageUrl = url(image);
        const defaultSize = SIZE_POSTFIX[SIZE.SMALL];

        expect(imageUrl.endsWith(defaultSize)).toBeTruthy();
      });

      it('should build url with large postfix if provided', () => {
        const imageUrl = url(image, SIZE.LARGE);
        const defaultSize = SIZE_POSTFIX[SIZE.LARGE];

        expect(imageUrl.endsWith(defaultSize)).toBeTruthy();
      });
    });
  });
});
