import { SIZE_POSTFIX, SIZE, PROTOCOL } from './constants';

export default (img, size = SIZE.SMALL) =>
  `${PROTOCOL}${img.uri}${SIZE_POSTFIX[size]}`;
