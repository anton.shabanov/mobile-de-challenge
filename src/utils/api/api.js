import request from './request';

export default {
  car: {
    show: request.bind(null, '/hiring-challenge.json'),
  },
};
