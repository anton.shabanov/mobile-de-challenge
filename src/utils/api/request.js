import { BASE_API_URL } from './constants';

const buildUrl = url => `${BASE_API_URL}${url}`;

export default async (url, method = 'GET', data = null, ...rest) => {
  const fullUrl = buildUrl(url);
  const body = data && JSON.stringify(data);
  const request = await fetch(fullUrl, { method, body, ...rest });
  const response = await request.json();
  return response;
};
