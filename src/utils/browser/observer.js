// eslint-disable-next-line import/prefer-default-export
export const hasObserver = () => 'IntersectionObserver' in window;
