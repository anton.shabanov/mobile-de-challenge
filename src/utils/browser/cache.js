const cache = {};

const raw = (key, value) => {
  if (value === null) return delete cache[key]; // if we want to clear cache
  if (!value) return cache[key]; // in case of read
  cache[key] = value; // otherwise just set the value
  return null;
};

export default {
  exists: key => raw(key) != null,
  set: (key, value) => raw(key, value),
  get: key => raw(key),
  remove: key => raw(key, null),
};
