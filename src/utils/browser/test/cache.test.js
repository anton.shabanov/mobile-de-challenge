import cache from 'src/utils/browser/cache';

describe('Utils', () => {
  describe('Browser', () => {
    describe('cache', () => {
      it('should be able to check for key existance', () => {
        expect(cache.exists('hello')).toBeFalsy();
      });

      it('should be able to retrieve value by given key', () => {
        expect(cache.get('hello')).toBeUndefined();
      });

      it('should be able to set the key/value pair', () => {
        cache.set('hello', 'world');

        expect(cache.exists('hello')).toBeTruthy();
        expect(cache.get('hello')).toBe('world');
      });

      it('should be able to remove key from storage', () => {
        cache.set('hello', 'world1');

        expect(cache.exists('hello')).toBeTruthy();
        expect(cache.get('hello')).toBe('world1');

        cache.remove('hello');

        expect(cache.exists('hello')).toBeFalsy();
        expect(cache.get('hello')).toBeUndefined();
      });
    });
  });
});
