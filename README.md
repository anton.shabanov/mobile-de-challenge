# Mobile De Frontend App

## Video Example

Please follow this link to see the app in action:

1. Without lazy loading: https://monosnap.com/file/yaDlKpWJx0KF28nc2kJnvnksXSwrSr
2. With lazy images (+ preloaded / I've left the preload option prior to browser regular image load stuff, but it could be simply tweaked with the one flag change): https://monosnap.com/file/5cpcPq2fid02aSrBe17SE7cbaXaHuW

## Installation

Make sure you're running node 11.12.0 or above.

```
$ cd project-dir
$ git clone repo-url .
$ yarn # to install packages
$ yarn dev # to start dev server
$ yarn prod # to build production ready html, bundle & assets
```

## Tests, Linters, Prettier

By default tests, linter & prettier are included inside the pre-commit hook, but if you want to run them manually please use these commands:

```
$ yarn pretty # to run prettier
$ yarn lint --fix # to run linter
$ yarn test # to run tests
$ yarn stylelint # to run styles linter
```

## Prod run

```
$ yarn serve # it'll compile assets & start small express static serving app (afterall visit localhost:3000 by default)
```

## Further improvements

Like a next imporvement could be added is the kind of "predict" preload. Meaning we could preload +1/+2/... next images if user is on the current slide, but for now this feature isn't supported (but shouldn't be that hard to add with the existing stuff).
