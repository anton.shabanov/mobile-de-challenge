const express = require('express');
const path = require('path');
const logger = require('./logger');

const app = express();
const port = process.env.PORT || 3000;
const distFolder = path.join(`${__dirname}/../dist/`);

app.use(express.static(distFolder));

app.get('/', (req, res) => res.sendFile(distFolder.join('index.html')));

app.listen(port, () =>
  logger.log(`Mobile De app is listening on port ${port}!`),
);
