const { info, log, warn } = console;

module.exports = { info, log, warn };
